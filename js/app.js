// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, onAuthStateChanged, signOut, sendPasswordResetEmail } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyB1PJ3lcJ4Oc4a8jvSKCL_LuBIDLYfbrZ0",
    authDomain: "proyecto-ejemplo-9b897.firebaseapp.com",
    projectId: "proyecto-ejemplo-9b897",
    storageBucket: "proyecto-ejemplo-9b897.appspot.com",
    messagingSenderId: "660202943408",
    appId: "1:660202943408:web:ff2dc6f95ad07b30ba5c75"
  };

 // Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();

document.querySelector('.btn-primary').addEventListener('click', function(e) {
    e.preventDefault();  // Evitar que el formulario se envíe de la forma convencional

    const email = document.getElementById('typeEmailX-2').value;
    const password = document.getElementById('typePasswordX-2').value;

    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Usuario ha iniciado sesión correctamente
            const user = userCredential.user;
            alert("Inicio de sesión exitoso!");
            window.location.href = '/html/menu.html';
        })
        .catch((error) => {
            // Error al iniciar sesión
            const errorCode = error.code;
            const errorMessage = error.message;
            alert("Error: " + errorMessage);
        });
});

